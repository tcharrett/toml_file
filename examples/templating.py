"""
The socket_controller3.config subpackage is a toml compatible configuration library.

This example shows how to use the Config object and templating interface for 
automatic type checking, default values, and value checking.
"""
import toml_file

#%%-----------------------------------------------------------------------------
# Using types / encoder to add comments but no checking!
#-------------------------------------------------------------------------------
template = config.types.TableType()
template.add_comment('Top level items')
template.add_comment(' - comment written before first section')
template.add_comment('inline', post=True)
template.add_comment(' - comment written after first section', post=True)

template['int'] = config.types.IntType(valid=[2,8,10,16], default=2) 
template['float'] = config.types.FloatType(min=0, max=255, default=1.0) 
template['string'] = config.types.StringType(default='test')

sub = config.types.TableType()
sub.add_comment('A sub table')
sub.add_comment('in line', post=True)
sub.add_comment('afterwards ', post=True)

template['section'] = sub
sub['test'] = config.types.IntType(default=1) 
sub['pi'] = config.types.FloatType(default=3.14) 


# write to file
data = {'int':1, 'float': 1}
s = config.encoder.dumps( data, template)
print(s)

#%%-----------------------------------------------------------------------------
# Using the config object - automatically checks values
#-------------------------------------------------------------------------------
c = config.Config()
c.template.add_comment('Top level items')
c.template.add_comment(' - comment written before first section')
c.template.add_comment('inline', post=True)
c.template.add_comment(' - comment written after first section', post=True)

c['int'] = config.types.IntType(valid=[2,8,10,16], default=2) 
c['float'] = config.types.FloatType(min=0, max=255, default=1.0) 
c['string'] = config.types.StringType(default='test')

sub = config.types.TableType()
sub.add_comment('A sub table')
sub.add_comment('in line', post=True)
sub.add_comment('afterwards ', post=True)

c['section'] = sub
sub['test'] = config.types.IntType(default=1) 
sub['pi'] = config.types.FloatType(default=3.14) 


# automatically checks values
c['int'] = 1.2      #this fails!